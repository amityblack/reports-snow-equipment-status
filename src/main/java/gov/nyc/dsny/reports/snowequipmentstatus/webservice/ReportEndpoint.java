package gov.nyc.dsny.reports.snowequipmentstatus.webservice;


import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

/**
 *
 */
@Component
@WebService(serviceName="ReportEndpoint", targetNamespace="http://report.reports.dsny.nyc.gov/")
public interface ReportEndpoint {

    @WebMethod(operationName = "getWorkUnits", action = "{http://snowcanman.reports.dsny.nyc.gov/}getWorkUnits")
    @WebResult(name = "workUnit", partName = "workUnit")
    List<String> getWorkUnits(@WebParam(name = "queryDate", targetNamespace = "http://snowcanman.reports.dsny.nyc.gov/") String queryDate);

}