package gov.nyc.dsny.reports.snowequipmentstatus.webservice;


import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ksafronov on 8/24/2016.
 */
@Slf4j
public class MockReportEndpointImpl implements ReportEndpoint {

     @Override
    public List<String> getWorkUnits(String queryDate) {
        return Arrays.asList(
            "Manhattan Boro",
            "Queens West Boro",
            "Queens East Boro"
        );
    }
}
